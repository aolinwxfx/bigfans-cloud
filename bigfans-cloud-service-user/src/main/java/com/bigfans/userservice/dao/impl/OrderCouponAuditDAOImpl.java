package com.bigfans.userservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.userservice.dao.OrderCouponAuditDAO;
import com.bigfans.userservice.model.OrderCouponAudit;
import org.springframework.stereotype.Repository;

@Repository
public class OrderCouponAuditDAOImpl extends MybatisDAOImpl<OrderCouponAudit> implements OrderCouponAuditDAO {

    @Override
    public OrderCouponAudit getByOrder(String orderId) {
        ParameterMap params = new ParameterMap();
        params.put("orderId" , orderId);
        params.put("reason" , OrderCouponAudit.REASON_ORDERCREATE);
        return null;
    }
}
