package com.bigfans.shippingservice.listener;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.model.event.payment.OrderPaidEvent;
import com.bigfans.shippingservice.api.clients.OrderServiceClient;
import com.bigfans.shippingservice.model.Delivery;
import com.bigfans.shippingservice.model.Order;
import com.bigfans.shippingservice.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@KafkaConsumerBean
public class OrderListener {

    @Autowired
    private DeliveryService deliveryService;
    @Autowired
    private OrderServiceClient orderServiceClient;

    @KafkaListener
    public void on(OrderPaidEvent event) {
        try {
            String orderId = event.getOrderId();
            Order order = orderServiceClient.getOrder(orderId).get();
            Delivery delivery = new Delivery();
            delivery.setAddress(order.getAddressDetail());
            delivery.setConsignee(order.getAddressConsignee());
            delivery.setEmail(order.getAddressEmail());
            delivery.setMobile(order.getAddressPhone());
            delivery.setUserId(order.getUserId());
            delivery.setOrderId(order.getId());
            delivery.setFreight(order.getFreight());
            deliveryService.create(delivery);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
