package com.bigfans.framework.cache;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2016年1月18日 上午10:53:56 
 * @version V1.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheEvict {
	
	/**
	 * 缓存key生成器
	 * @return
	 */
	Class<? extends CacheKeyGenerator> keyGenerator() default CacheKeyGenerator.class;
	
	/**
	 * 设置缓存key，如果设置了这个选项就不会通过@CacheKeyGenerator 来生成key值。
	 * @return
	 */
	String key() default "";
}
