package com.bigfans.searchservice.api;

import com.bigfans.framework.es.request.AggregationResult;
import com.bigfans.framework.model.FTSPageBean;
import com.bigfans.framework.model.PageContext;
import com.bigfans.framework.utils.CollectionUtils;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.searchservice.api.clients.CatalogServiceClient;
import com.bigfans.searchservice.model.*;
import com.bigfans.searchservice.model.vo.SearchFilter;
import com.bigfans.searchservice.model.vo.SearchFilterItem;
import com.bigfans.searchservice.service.BrandService;
import com.bigfans.searchservice.service.CategoryService;
import com.bigfans.searchservice.service.ProductSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * 品牌搜索服务
 *
 * @author lichong
 */
//@RestController
public class ProductSearchController_copy extends BaseController {

//    @Autowired
//    private ProductSearchService productSearchService;
//    @Autowired
//    private CategoryService categoryService;
//    @Autowired
//    private BrandService brandService;
//    @Autowired
//    private CatalogServiceClient catalogServiceClient;
//
//    /**
//     * @param keyword
//     * @return
//     */
//    // /search?q=xxx&b=xxx&p=xxx_xxx&o=xxx
//    // 格式:属性id_属性值id,属性id_属性值id
//    // /search?params=1234_4321,222_333
//    @GetMapping(value = "/search") //get方式不支持传递中文,需要进行转码
//    public RestResponse search(
//            @RequestParam(name = "q", required = false) String keyword,
//            @RequestParam(name = "catId", required = false) String catId,
//            @RequestParam(name = "brandId", required = false) String brandId,
//            @RequestParam(name = "filters", required = false) String filters,
//            @RequestParam(name = "priceRange", required = false) String priceRange,
//            @RequestParam(name = "orderBy", required = false) String orderBy,
//            @RequestParam(name = "cp", required = false, defaultValue = "1") Integer currentPage
//    ) throws Exception {
//
//        PageContext.setCurrentPage(currentPage.longValue());
//        // 设置参数
//        ProductSearchRequest searchRequest = new ProductSearchRequest();
//        searchRequest.setKeyword(keyword);
//        searchRequest.setCategoryId(catId);
//        searchRequest.setBrandId(brandId);
//        if (StringHelper.isNotEmpty(filters)) {
//            Map<String, Object> attrMap = new HashMap<>();
//            String[] filterArr = filters.split(",");
//            for (String filter : filterArr) {
//                String[] attrVal = filter.split("_");
//                attrMap.put(attrVal[0], attrVal[1]);
//            }
//            searchRequest.setAttrs(attrMap);
//        }
//        if (StringHelper.isNotEmpty(priceRange)) {
//            String[] split = priceRange.split("-");
//            searchRequest.setMinPrice(split[0]);
//            searchRequest.setMaxPrice(split[1]);
//        }
//
//        // 执行全文检索
//        FTSPageBean<Product> productPage = productSearchService.searchProduct(searchRequest, PageContext.getStart().intValue(), 60);
//
//        Map<String, List<AggregationResult>> aggMap = productPage.getAggregationMap();
//        // 添加类别filter
//        List<AggregationResult> catAggList = aggMap.get("category");
//        SearchFilter catFilter = this.buildCategoryFilter(catAggList);
//
//        // 添加品牌filter
//        List<AggregationResult> brandAggList = aggMap.get("brand");
//        SearchFilter brandFilter = this.buildBrandFilter(brandAggList);
//
//        // 添加商品属性filter
//        List<AggregationResult> attrAggList = aggMap.get("attributes");
//        List<SearchFilter> attrFilters = this.buildAttributeFilter(attrAggList);
//
//        Map<String , Object> data = new HashMap<>();
//        data.put("products" , productPage.getData());
//        data.put("recordCount" , productPage.getTotalCount());
//        data.put("pageCount" , productPage.getTotalPageCount());
//        data.put("catFilter" , catFilter);
//        data.put("brandFilter" , brandFilter);
//        data.put("attrFilters" , attrFilters);
//        return RestResponse.ok(data);
//    }
//
////    private List<SearchFilter> buildFilter(Map<String, List<AggregationResult>> aggMap) throws Exception {
////        List<SearchFilter> filters = new ArrayList<>();
////        // 添加类别filter
////        List<AggregationResult> catAggList = aggMap.get("category");
////        filters.add(this.buildCategoryFilter(catAggList));
////
////        // 添加品牌filter
////        List<AggregationResult> brandAggList = aggMap.get("brand");
////        filters.add(this.buildBrandFilter(brandAggList));
////
////        // 添加商品属性filter
////        List<AggregationResult> attrAggList = aggMap.get("attributes");
////        filters.addAll(this.buildAttributeFilter(attrAggList));
////        return filters;
////    }
//
//    private List<SearchFilter> buildAttributeFilter(List<AggregationResult> attrAggList) throws Exception {
//        List<SearchFilter> attrFilters = new ArrayList<>();
//        if (CollectionUtils.isNotEmpty(attrAggList)) {
//            List<String> attrValueIdList = new ArrayList<>();
//            for (AggregationResult attrAgg : attrAggList) {
//                // name是attrValueId的值
//                String attrValueId = attrAgg.getKey();
//                attrValueIdList.add(attrValueId);
//            }
//            // 根据聚合的属性ID的结果,去数据库中查询出对应的数据
//            CompletableFuture<List<AttributeValue>> future = catalogServiceClient.getAttributesByIdList(attrValueIdList);
//            List<AttributeValue> attrValList = future.get();
//            Map<String, SearchFilter> filterOptionMap = new HashMap<>();
//
//            for (AttributeValue attrVal : attrValList) {
//                String optId = attrVal.getOptionId();
//                if (filterOptionMap.containsKey(optId)) {
//                    SearchFilter filter = filterOptionMap.get(optId);
//                    SearchFilterItem item = new SearchFilterItem();
//                    item.setValue(attrVal.getValue());
//                    item.setValueId(attrVal.getId());
//                    filter.addValue(item);
//                } else {
//                    SearchFilter filter = new SearchFilter();
//                    filter.setOptionName(attrVal.getOptionName());
//                    SearchFilterItem item = new SearchFilterItem();
//                    item.setValue(attrVal.getValue());
//                    item.setValueId(attrVal.getId());
//                    filter.addValue(item);
//                    filterOptionMap.put(optId, filter);
//                }
//            }
//        }
//        return attrFilters;
//    }
//
//    private SearchFilter buildCategoryFilter(List<AggregationResult> catAggList) throws Exception {
//        SearchFilter catFilter = null;
//        if (CollectionUtils.isNotEmpty(catAggList)) {
//            catFilter = new SearchFilter();
//            catFilter.setOptionName("类别");
//            for (AggregationResult aggregationResult : catAggList) {
//                String catId = aggregationResult.getKey();
//                Category cat = categoryService.getById(catId);
//                SearchFilterItem item = new SearchFilterItem();
//                item.setValue(cat.getName());
//                item.setValueId(catId);
//                catFilter.addValue(item);
//            }
//        }
//        return catFilter;
//    }
//
//    private SearchFilter buildBrandFilter(List<AggregationResult> brandAggList) throws Exception {
//        SearchFilter brandFilter = null;
//        if (CollectionUtils.isNotEmpty(brandAggList)) {
//            brandFilter = new SearchFilter();
//            brandFilter.setOptionName("品牌");
//            for (AggregationResult aggregationResult : brandAggList) {
//                String brandId = aggregationResult.getKey();
//                Brand brand = brandService.getById(brandId);
//                SearchFilterItem item = new SearchFilterItem();
//                item.setValue(brand.getName());
//                item.setValueId(brandId);
//                brandFilter.addValue(item);
//            }
//        }
//        return brandFilter;
//    }
}
