package org.bigfans.paymentservice;

import com.bigfans.framework.kafka.KafkaTemplate;
import com.bigfans.model.event.payment.OrderPaidEvent;
import com.bigfans.paymentservice.PayServiceApp;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Unit test for simple PayServiceApp.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=PayServiceApp.class)
public class AppTest extends TestCase
{
    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Test
    public void testPublish(){
        kafkaTemplate.send(new OrderPaidEvent("110"));
    }

}
