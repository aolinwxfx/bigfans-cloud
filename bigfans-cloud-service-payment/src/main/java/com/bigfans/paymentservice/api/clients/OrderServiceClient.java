package com.bigfans.paymentservice.api.clients;

import com.bigfans.api.clients.ServiceRequest;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.paymentservice.PaymentApplications;
import com.bigfans.paymentservice.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-25 下午4:21
 **/
@Component
public class OrderServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Order> getOrder(String orderId){
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate, PaymentApplications.getFunctionalUser());
            Map data = serviceRequest.get(Map.class , "http://order-service/orders/{id}" , orderId);
            Order order = new Order();
            BeanUtils.mapToModel(data , order);
            return order;
        });
    }
}
