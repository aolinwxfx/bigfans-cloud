package com.bigfans.paymentservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.paymentservice.dao.PaymentDAO;
import com.bigfans.paymentservice.model.Payment;
import org.springframework.stereotype.Repository;


/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月5日下午6:35:59
 *
 */
@Repository(PaymentDAOImpl.BEAN_NAME)
public class PaymentDAOImpl extends MybatisDAOImpl<Payment> implements PaymentDAO {

	public static final String BEAN_NAME = "paymentDAO";

	@Override
	public Payment getByOrder(String uid, String orderId) {
		ParameterMap params = new ParameterMap();
		params.put("buyerId", uid);
		params.put("orderId", orderId);
		return getSqlSession().selectOne(className + ".load", params);
	}

	@Override
	public Payment getByTradeNo(String tradeNo) {
		ParameterMap params = new ParameterMap();
		params.put("tradeNo", tradeNo);
		return getSqlSession().selectOne(className + ".load", params);
	}
}
