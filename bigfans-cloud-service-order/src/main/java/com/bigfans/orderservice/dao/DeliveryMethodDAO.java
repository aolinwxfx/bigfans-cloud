package com.bigfans.orderservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.orderservice.model.DeliveryMethod;

/**
 * 
 * @Description:配送类型DAO操作
 * @author lichong
 * 2015年4月6日上午9:58:12
 *
 */
public interface DeliveryMethodDAO extends BaseDAO<DeliveryMethod> {

}
