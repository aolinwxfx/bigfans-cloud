package com.bigfans.orderservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.framework.model.PageBean;
import com.bigfans.framework.model.PageContext;
import com.bigfans.orderservice.dao.OrderItemDAO;
import com.bigfans.orderservice.model.OrderItem;
import com.bigfans.orderservice.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * 
 * @Description:订单条目
 * @author lichong
 * 2015年1月17日下午8:01:29
 *
 */
@Service(OrderItemServiceImpl.BEAN_NAME)
public class OrderItemServiceImpl extends BaseServiceImpl<OrderItem> implements OrderItemService {

	public static final String BEAN_NAME = "orderItemService";
	
	private OrderItemDAO orderItemDAO;
	
	@Autowired
	public OrderItemServiceImpl(OrderItemDAO orderItemDAO) {
		super(orderItemDAO);
		this.orderItemDAO = orderItemDAO;
	}
	
	/**
	 * 获取所有订单条目
	 * @param orderId
	 * @return
	 * @throws Exception 
	 */
	@Transactional(readOnly = true)
	public List<OrderItem> listByUserOrder(String userId , String orderId) throws Exception {
		return orderItemDAO.listByUserOrder(userId, orderId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OrderItem> listByOrder(String orderId) throws Exception {
		return orderItemDAO.listByOrder(orderId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OrderItem> listUnCommentedItems(String userId, String orderId) throws Exception {
		return orderItemDAO.listUnCommentedByOrder(userId , orderId);
	}

	@Override
	@Transactional
	public void updateStatusToCommented(String orderItemId) throws Exception {
		OrderItem orderItem = new OrderItem();
		orderItem.setId(orderItemId);
		orderItem.setCommentStatus(OrderItem.COMMENT_STATUS_COMMENTED);
		super.update(orderItem);
	}
}
