package com.bigfans.catalogservice.service.sku;

import com.bigfans.catalogservice.model.StockLog;
import com.bigfans.framework.dao.BaseService;

import java.util.List;

public interface StockLogService extends BaseService<StockLog> {

    List<StockLog> listByOrder(String orderId) throws Exception;

    List<StockLog> listOrderStockOutLog(String orderId) throws Exception;

    Integer countByOrder(String orderId) throws Exception;

}
