package com.bigfans.catalogservice.service.product;

import com.bigfans.catalogservice.model.ImageGroup;
import com.bigfans.catalogservice.model.ProductImage;
import com.bigfans.framework.dao.BaseService;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * @author lichong
 * @version V1.0
 * @Title:
 * @Description:
 * @date 2015年12月24日 下午9:12:47
 */
public interface ProductImageService extends BaseService<ProductImage> {

    void upload(ProductImage image, InputStream is) throws Exception;

    void uploadThumb(ProductImage image, InputStream is) throws Exception;

    void upload(ProductImage image, File srcFile) throws Exception;

    /**
     * 显示商品相关的图片
     *
     * @param productId
     * @return
     */
    List<ImageGroup> listProductImages(String productId) throws Exception;

    ProductImage getThumb(String prodId) throws Exception;

}
