package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description:商品标签
 * @author lichong 
 * 2015年6月6日上午11:29:21
 *
 */
@Table(name="Product_Tag")
public class ProductTagEntity extends AbstractModel {

	public String getModule() {
		return "ProductTag";
	}
	
	private static final long serialVersionUID = -6112016318532673405L;

	@Column(name="prod_id")
	private String prodId;
	@Column(name="tag_id")
	private String tagId;
	
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

}
