package com.bigfans.systemservice;

import com.bigfans.framework.exception.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ApplicationExceptionHandler extends GlobalExceptionHandler{

}
