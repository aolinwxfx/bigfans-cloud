package com.bigfans.model.event.order;

import java.util.Map;

/**
 * @author lichong
 * @create 2018-02-25 下午2:48
 **/
public class OrderExpiredItemTakenBackToCartEvent {

    private Map<String , Integer> expiredItems;

    public OrderExpiredItemTakenBackToCartEvent(Map<String , Integer> expiredItems) {
        this.expiredItems = expiredItems;
    }
}
